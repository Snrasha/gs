package src.data.utils;


public class GladiatorSociety_test {
    public static void main(String[] args){

        float a=0;
        float b=0;
        System.out.println("a(i+1)= a(i) + 0.5f - (1f-0.5f)  =/=  b(i+1)= b(i) + 0.2f - (1f-0.8f)");
        for(int i=0;i<10000000;i++){
            a=a + 0.5f - (1f-0.5f);
            b=b + 0.2f - (1f-0.8f);
            if((i%1000000)==0){
                    System.out.println(i+"th iteration : a("+i+")="+a+"  =/=  b("+i+")="+b);
            }
        }
    }
    
    
}
